//
//  Shape.h
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import <Foundation/Foundation.h>
#define RED 1
#define GREEN 2
#define BLUE 3

@interface Shape : NSObject

@property int color;
@property NSString *name;

-(NSString *)printColor;

-(int) selfArea;

@end
