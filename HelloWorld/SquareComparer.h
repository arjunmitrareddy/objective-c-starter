//
//  SquareComparer.h
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Square.h"

@interface SquareComparer : NSObject

-(Square *) getBigger: (Square *) s1 and: (Square *) s2;

@end
