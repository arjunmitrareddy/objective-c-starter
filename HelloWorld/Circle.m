//
//  Circle.m
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import "Circle.h"

@implementation Circle

@synthesize size;
@synthesize name;

-(id) initWithSize:(int)s {
    self = [super init];
    
    if (self) {
        size = s;
    }
    
    return self;
}

-(int) selfArea {
    return 2*3.14*size;
}

@end
