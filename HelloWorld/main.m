//
//  main.m
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Square.h"
#import "Circle.h"
#import "SquareComparer.h"


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Shape *s1 = [[Square alloc] initWithSize: 10];
        Square *s2 = [[Square alloc] initWithSize: 20];
        Shape *c1 = [[Circle alloc] initWithSize:30];
        SquareComparer *sc = [[SquareComparer alloc] init];
        
        s1.name = @"Square With Side 1000";
        s2.name = @"Sqaure with Side 2000";
        s1.color = RED;
        c1.color = GREEN;
        c1.name = @"Circle One";
        
        @try {
            NSLog(@"Area of the custom square is %i", [s2 areaOfCustomSquare:5 width: 6]);
        }
        @catch (NSException *exception) {
            NSLog(@"Failed to Create Square Because - %@", exception.reason);
        }
        @finally {
            NSLog(@"Please make sure the Width & Length of the Square are same");
        }
        NSLog(@"Area of the self square is %i", [s1 selfArea]);
        NSLog(@"Bigger Square is %@", [sc getBigger:s1 and:s2].name);
        
        NSLog(@"Color of Square s1 %@", [s1 printColor]);
        NSLog(@"Color of Circle c1 %@", [c1 printColor]);
        NSLog(@"Name of Square s1 %@", s1.name);
        NSLog(@"Area of Circle c1 %d", [c1 selfArea]);
    
        
        
    }
    return 0;
}
