//
//  Square.m
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import "Square.h"

@implementation Square

@synthesize size;

-(id) initWithSize:(int)s {
    self = [super init];
    
    if (self) {
        size = s;
    }
    
    return self;
}

-(int) areaOfCustomSquare:(int) l width:(int) w {
    if (l != w) {
        @throw [NSException exceptionWithName:@"Invalid Dimensions" reason:@"Lenght & Width Should Match for Square" userInfo:nil];
    }
    return l * w;
}

-(int) selfArea {
    return size * size;
}

@end
