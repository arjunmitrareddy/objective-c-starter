//
//  Shape.m
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import "Shape.h"

@implementation Shape

@synthesize color;
@synthesize name;

-(NSString *) printColor
{
    switch (color) {
        case RED:
            return @"Red";
            break;
            
        case GREEN:
            return @"Green";
            break;
            
        case BLUE:
            return @"Blue";
            break;
            
        default:
            return @"Unknown";
            break;
    }
}

-(int) selfArea {
    return -1;
}
@end
