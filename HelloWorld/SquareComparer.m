//
//  SquareComparer.m
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import "SquareComparer.h"
#import "Square.h"

@implementation SquareComparer

-(Square *) getBigger:(Square *)s1 and:(Square *)s2 {
    if (s1.size > s2.size) {
        return s1;
    }
    else {
        return s2;
    }
}

@end
