//
//  Square.h
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape.h"

@interface Square : Shape
{
}

@property int size;

-(id) initWithSize: (int) s;

-(int) areaOfCustomSquare: (int) l width: (int) w;

-(int) selfArea;

@end
