//
//  Circle.h
//  HelloWorld
//
//  Created by Arjun Donthala on 5/25/17.
//  Copyright © 2017 Arjun Donthala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape.h"

@interface Circle : Shape
{
}

@property int size;
@property NSString *name;

-(id) initWithSize: (int) s;

-(int) selfArea;

@end
